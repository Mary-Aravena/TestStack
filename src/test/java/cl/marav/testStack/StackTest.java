package cl.marav.testStack;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackTest {
	MyStack pila;
	String result;
	String result2;
	@Before
	public void setUp(){
		
		pila= new MyStack();
	}

	@Test
	public void PilaVaciatestReturnEsVacia() {
		result= pila.agrego();
		assertEquals("Es vacia", result);
	}
	
	@Test
	public void PilaAdd1ReturnStackNoVacio(){
		result= pila.agrego(1);
		assertEquals("No es vacia", result);
	}
	
	@Test
	public void PilaAdd1_2ReturnStackNoVacio(){
		result= pila.agrego(1);
		result= pila.agrego(2);
		assertEquals("No es vacia", result);
	}
	@Test
	public void pilaAdd1_2ReturnTamañoEsDos(){
		pila.addValor(1);
		pila.addValor(2);
		result= pila.mySize();

		assertEquals("2", result);
	}
	
	@Test
	public void pilaAdd1Pop1_Return1(){
		pila.addValor(1);
		result= pila.myPop();
		assertEquals("1",result);
	}

	@Test
	public void pilaAdd1_2Pop1_Return2(){
		pila.addValor(1);
		pila.addValor(2);
		result= pila.myPop();
		assertEquals("2",result);
		
	}
	
	@Test
	public void pilaAdd3_4_Pop2_Return4_3(){
		pila.addValor(3);
		pila.addValor(4);
		result2= pila.myPop(2);
		assertEquals("4, 3", result2);
	}
	
	@Test
	public void pilaAdd1_Top_Return1(){
		pila.addValor(1);
		result= pila.myTop();
		assertEquals("1",result);
	}
	@Test
	public void pilaAdd1_2_Top_Return1(){
		pila.addValor(1);
		pila.addValor(2);
		result= pila.myTop();
		assertEquals("2",result);
	}
}
