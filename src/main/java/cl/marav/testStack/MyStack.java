package cl.marav.testStack;

import java.util.ArrayList;

public class MyStack {
	ArrayList<String> pila;
	int i;
	
	MyStack(){
		pila= new ArrayList<String>();
		i=0;
	}

	public String agrego(){
		return "Es vacia";
	}
	public String agrego(int valor){
		return "No es vacia";
	}

	public void addValor(int valor){
		pila.add(Integer.toString(valor));
		i =pila.size();
	}
	
	public String mySize(){
		String retorno = Integer.toString(i);
		return retorno;
	}

	public String myPop(){
		String temp= pila.remove(i-1);
		i =pila.size();
		return temp;
	}
	
	public String myPop(int valor){
		String temp= "";
		for(int f=0; f<valor;f++){
			if(f!=0)
			temp= temp+", "+myPop();
			else
				temp=myPop();
		}
		return temp;
	}
	
	public String myTop(){
		return pila.get(i-1);
	}
}
